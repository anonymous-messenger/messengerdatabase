drop procedure if exists getNMessagesBeforeId;

delimiter $$
create procedure getNMessagesBeforeId(
	in `count` int,
    in `end_id` int,
    in `channel_id` int
)
begin
	select `user`.`username`, `message`.`text`, `message`.`time` from `message` 
    inner join `user` on `user`.`iduser` = `message`.`idsender`
    where
    `message`.`idmessage` < `end_id` and `message`.`idchannel` = `channel_id`
    order by `message`.`idmessage`
    limit `count`;
end$$
delimiter ;

