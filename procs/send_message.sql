drop procedure if exists createFriendShip;

delimiter $$
create procedure sendMessage(
	in `sender` varchar(256),
    in `text` longtext,
    in `channel_id` int
)
begin
    declare `sender_id` int;
    set time_zone = '+00:00';
    call getIdFromUserName(`sender`, `sender_id`);
    
    
    
	insert into `message`(`idsender`, `text`, `idchannel`) values
    (`sender_id`, `text`, `channel_id`);

	

end$$
delimiter ;

