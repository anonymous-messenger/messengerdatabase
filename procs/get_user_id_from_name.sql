drop procedure if exists getIdFromUserName;

delimiter $$
create procedure getIdFromUserName(
	in `_name_user` varchar(256),
    out `_id_user` int
)
begin
	select `user`.`iduser` from `user` 
    where `user`.`username` = `_name_user` into `_id_user`;
end$$
delimiter ;