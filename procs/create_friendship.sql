drop procedure if exists createFriendShip;

delimiter $$
create procedure createFriendShip(
	in `user1` varchar(256),
    in `user2` varchar(256)
)
begin
	declare `user1id`, `user2id`, `channel_id` int;
    call getIdFromUserName(`user1`, `user1id`);
    call getIdFromUserName(`user2`, `user2id`);
    
    insert into `friends`(`user_from`, `user_to`) values
    (`user1id`, `user2id`),
    (`user2id`, `user1id`);
    
    insert into `channel`(`idcom_category`, `name`) values
    ('1', concat(`user1`, concat(' ', `user2`)));
    
    set `channel_id` = last_insert_id();
    
    insert into `user_has_channel`(`idchannel`, `iduser`) values
    (`channel_id`, `user2id`);
    
    insert into `user_has_channel`(`idchannel`, `iduser`) values
    (`channel_id`, `user1id`);
end$$
delimiter ;
