drop procedure if exists getUserInfo;

delimiter $$
create procedure getUserInfo(
	in `_name` varchar(256)
)
begin
    select `user`.`age`, `gender`.`name`, `user`.`score` from `user` 
    inner join `gender` on `gender`.`idgender` = `user`.`idgender`
    where `user`.`username` = `_name`;
end$$
delimiter ;

