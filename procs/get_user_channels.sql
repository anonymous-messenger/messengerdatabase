drop procedure if exists getUserChannels;

delimiter $$
create procedure getUserChannels(
	in `_name` varchar(256)
)
begin
	declare `userid` int;
    select `user`.`iduser` from `user` where `user`.`username` = `_name` into `userid`;

	select `channel`.`idchannel`, `channel`.`name` from `user_has_channel` 
    inner join `channel` on `channel`.`idchannel` = `user_has_channel`.`idchannel`
    where `user_has_channel`.`iduser` = `userid`;
end$$
delimiter ;

