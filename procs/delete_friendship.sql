drop procedure if exists deleteFriendShip;

delimiter $$
create procedure deleteFriendShip(
	in `user1` varchar(256),
    in `user2` varchar(256)
)
begin
	declare `user1id`, `user2id`, `channel_id` int;
    call getIdFromUserName(`user1`, `user1id`);
    call getIdFromUserName(`user2`, `user2id`);
    
    delete from `friends` where
    (`user_from` = `user1id` and `user_to` = `user2id`) or
    (`user_from` = `user2id` and `user_to` = `user1id`);
    
    select a.idchannel from 
    user_has_channel as a 
    inner join
	user_has_channel as b 
    on a.idchannel = b.idchannel 
    where
	a.iduser = `user1id` and b.iduser = `user2id` 
    into `channel_id`;
    
    delete from `channel` 
    where `channel`.`idchannel` = `channel_id`;
    
    delete from `user_has_channel` 
    where `user_has_channel`.`idchannel` = `channel_id`;
end$$
delimiter ;
