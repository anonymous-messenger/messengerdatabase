drop procedure if exists checkUserName;

delimiter $$
create procedure checkUserName(
	in `_name` varchar(256)
)
begin
    if exists (select `user`.`iduser` from `user` where `user`.`username` = `_name`) then
    select "1";
    else
    select "0";
    end if;
end$$
delimiter ;

