drop procedure if exists getPassHashData;

delimiter $$
create procedure getPassHashData(
	in `_name` varchar(256)
)
begin
	declare `id` int;
    
    select `user`.`idpass_hash` from `user` where `user`.`username` = `_name` into `id`;
    
    select `salt`, `iteration`, `prf`, `bytes_requested` from `pass_hash`
    where `pass_hash`.`idpass_hash` = `id`;
end$$
delimiter ;

