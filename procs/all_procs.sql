drop procedure if exists connectToOneOnOneChat;

delimiter $$
create procedure connectToOneOnOneChat(
	in `_name_user` varchar(256),
	in `_name_category` varchar(256)
)
begin
	declare `user_id`, `category_id`, `metrics_id` int;
    set time_zone = '+00:00';
    call getIdFromUserName(`_name_user`, `user_id`);
    call getIdFromCategoryName(`_name_category`, `category_id`);
    select `idvisited_categories` from `visited_categories`
    where `idcom_category` = `category_id` and `iduser` = `user_id`
    into `metrics_id`;
    
    if `metrics_id` is not null then
		update `visited_categories` set
        `last_visit` = now() 
        where `metrics_id` = `idvisited_categories`;
        
		select "1";
    else
		insert into `visited_categories`(`idcom_category`, `iduser`)
        values (`category_id`, `user_id`);
        
		select "1";
    end if;
    
    
end$$
delimiter ;













drop procedure if exists disconnectFromOneOnOneChat;

delimiter $$
create procedure disconnectFromOneOnOneChat(
	in `_name_user` varchar(256),
	in `_name_category` varchar(256)
)
begin
	declare `user_id`, `category_id`, `metrics_id` int;
    declare `leave_time` datetime;
    set time_zone = '+00:00';
    call getIdFromUserName(`_name_user`, `user_id`);
    call getIdFromCategoryName(`_name_category`, `category_id`);
    select `idvisited_categories` from `visited_categories`
    where `idcom_category` = `category_id` and `iduser` = `user_id`
    into `metrics_id`;
    
    if `metrics_id` is not null then
		set `leave_time` = now();
        
        
		update `visited_categories` set
        `last_leave` = `leave_time`,
        `total_time` = ADDDATE(`total_time`, interval time_to_sec(TIMEDIFF(`leave_time`, `last_visit`)) second)
        where `metrics_id` = `idvisited_categories`;
        
		select "1";
    else
		
		select "0";
    end if;
    
    
end$$
delimiter ;












drop procedure if exists getIdFromCategoryName;

delimiter $$
create procedure getIdFromCategoryName(
	in `_name_cat` varchar(256),
    out `_id_cat` int
)
begin
	select `com_category`.`idcom_category` from `com_category` 
    where `com_category`.`name` = `_name_cat` into `_id_cat`;
    
end$$
delimiter ;








drop procedure if exists getIdFromUserName;

delimiter $$
create procedure getIdFromUserName(
	in `_name_user` varchar(256),
    out `_id_user` int
)
begin
	select `user`.`iduser` from `user` 
    where `user`.`username` = `_name_user` into `_id_user`;
end$$
delimiter ;
















drop procedure if exists `register`;

delimiter $$
create procedure `register`(
	in `_name` varchar(128),
    in `_age` int,
    in `_gender` varchar(45),
    in `_pass_hash` varchar(256),
    in `_salt` varchar(128),
    in `_iteration` int,
    in `_prf` int,
    in `_bytes_requested` int
)
begin
	if exists (select `username` from `user` where `user`.`username` = `_name`) then
		select "0";
    else
		insert into `pass_hash` (`hash_result`, `salt`, `iteration`, `prf`, `bytes_requested`)
        values (`_pass_hash`, `_salt`, `_iteration`, `_prf`, `_bytes_requested`);
        
        insert into `user` (`username`, `idpass_hash`, `age`, `idgender`)
        values (`_name`, last_insert_id(), `_age`, 
			(select `gender`.`idgender` from `gender` where `gender`.`name` = `_gender`)
		);
        
        select "1";
    end if;
end$$
delimiter ;
















drop procedure if exists auth;

delimiter $$
create procedure auth(
	in `_name` varchar(128),
    in `_hash_result` varchar(256)
)
begin
	if exists (
    select `username`, `hash_result` from
    `user` as `u`  inner join `pass_hash` as `p` 
    on `u`.`idpass_hash` = `p`.`idpass_hash`
    where `p`.`hash_result` = `_hash_result` and `u`.`username` = `_name`
    ) then
		select "1";
	else
        select "0";
	end if;
end$$
delimiter ;













drop procedure if exists createFriendShip;

delimiter $$
create procedure createFriendShip(
	in `user1` varchar(256),
    in `user2` varchar(256)
)
begin
	declare `user1id`, `user2id`, `channel_id` int;
    call getIdFromUserName(`user1`, `user1id`);
    call getIdFromUserName(`user2`, `user2id`);
    
    insert into `friends`(`user_from`, `user_to`) values
    (`user1id`, `user2id`),
    (`user2id`, `user1id`);
    
    insert into `channel`(`idcom_category`, `name`) values
    ('1', concat(`user1`, concat(' ', `user2`)));
    
    set `channel_id` = last_insert_id();
    
    insert into `user_has_channel`(`idchannel`, `iduser`) values
    (`channel_id`, `user2id`);
    
    insert into `user_has_channel`(`idchannel`, `iduser`) values
    (`channel_id`, `user1id`);
end$$
delimiter ;












drop procedure if exists deleteFriendShip;

delimiter $$
create procedure deleteFriendShip(
	in `user1` varchar(256),
    in `user2` varchar(256)
)
begin
	declare `user1id`, `user2id`, `channel_id` int;
    call getIdFromUserName(`user1`, `user1id`);
    call getIdFromUserName(`user2`, `user2id`);
    
    delete from `friends` where
    (`user_from` = `user1id` and `user_to` = `user2id`) or
    (`user_from` = `user2id` and `user_to` = `user1id`);
    
    select a.idchannel from 
    user_has_channel as a 
    inner join
	user_has_channel as b 
    on a.idchannel = b.idchannel 
    where
	a.iduser = `user1id` and b.iduser = `user2id` 
    into `channel_id`;
    
    delete from `channel` 
    where `channel`.`idchannel` = `channel_id`;
    
    delete from `user_has_channel` 
    where `user_has_channel`.`idchannel` = `channel_id`;
end$$
delimiter ;









drop procedure if exists getPassHashData;

delimiter $$
create procedure getPassHashData(
	in `_name` varchar(256)
)
begin
	declare `id` int;
    
    select `user`.`idpass_hash` from `user` where `user`.`username` = `_name` into `id`;
    
    select `salt`, `iteration`, `prf`, `bytes_requested` from `pass_hash`
    where `pass_hash`.`idpass_hash` = `id`;
end$$
delimiter ;









drop procedure if exists checkUserName;

delimiter $$
create procedure checkUserName(
	in `_name` varchar(256)
)
begin
    if exists (select `user`.`iduser` from `user` where `user`.`username` = `_name`) then
    select "1";
    else
    select "0";
    end if;
end$$
delimiter ;











drop procedure if exists getUserInfo;

delimiter $$
create procedure getUserInfo(
	in `_name` varchar(256)
)
begin
    select `user`.`age`, `gender`.`name`, `user`.`score` from `user` 
    inner join `gender` on `gender`.`idgender` = `user`.`idgender`
    where `user`.`username` = `_name`;
end$$
delimiter ;











drop procedure if exists sendMessage;

delimiter $$
create procedure sendMessage(
	in `sender` varchar(256),
    in `text` longtext,
    in `channel_id` int
)
begin
    declare `sender_id` int;
    set time_zone = '+00:00';
    call getIdFromUserName(`sender`, `sender_id`);
    
    
    
	insert into `message`(`idsender`, `text`, `idchannel`) values
    (`sender_id`, `text`, `channel_id`);

	

end$$
delimiter ;













drop procedure if exists getNMessagesBeforeId;

delimiter $$
create procedure getNMessagesBeforeId(
	in `count` int,
    in `end_id` int,
    in `channel_id` int
)
begin
	select `user`.`username`, `message`.`text`, `message`.`time` from `message` 
    inner join `user` on `user`.`iduser` = `message`.`idsender`
    where
    `message`.`idmessage` < `end_id` and `message`.`idchannel` = `channel_id`
    order by `message`.`idmessage`
    limit `count`;
end$$
delimiter ;












drop procedure if exists getUserChannels;

delimiter $$
create procedure getUserChannels(
	in `_name` varchar(256)
)
begin
	declare `userid` int;
    select `user`.`iduser` from `user` where `user`.`username` = `_name` into `userid`;

	select `channel`.`idchannel`, `channel`.`name` from `user_has_channel` 
    inner join `channel` on `channel`.`idchannel` = `user_has_channel`.`idchannel`
    where `user_has_channel`.`iduser` = `userid`;
end$$
delimiter ;












drop procedure if exists setUserMark;

delimiter $$
create procedure setUserMark(
	in `_name` varchar(256),
    in `_mark` int
)
begin
	
	update `user` set
    `user`.`score` = `_mark`
	where `user`.`username` = `_name`;
end$$
delimiter ;