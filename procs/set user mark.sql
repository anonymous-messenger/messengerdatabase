drop procedure if exists setUserMark;

delimiter $$
create procedure setUserMark(
	in `_name` varchar(256),
    in `_mark` int
)
begin
	
	update `user` set
    `user`.`score` = `_mark`
	where `user`.`username` = `_name`;
end$$
delimiter ;