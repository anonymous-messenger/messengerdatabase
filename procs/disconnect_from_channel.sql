drop procedure if exists disconnectFromOneOnOneChat;

delimiter $$
create procedure disconnectFromOneOnOneChat(
	in `_name_user` varchar(256),
	in `_name_category` varchar(256)
)
begin
	declare `user_id`, `category_id`, `metrics_id` int;
    declare `leave_time` datetime;
    call getIdFromUserName(`_name_user`, `user_id`);
    call getIdFromCategoryName(`_name_category`, `category_id`);
    select `idvisited_categories` from `visited_categories`
    where `idcom_category` = `category_id` and `iduser` = `user_id`
    into `metrics_id`;
    
    if `metrics_id` is not null then
		set `leave_time` = now();
        
        
		update `visited_categories` set
        `last_leave` = `leave_time`,
        `total_time` = ADDDATE(`total_time`, interval time_to_sec(TIMEDIFF(`leave_time`, `last_visit`)) second)
        where `metrics_id` = `idvisited_categories`;
        
		select "1";
    else
		
		select "0";
    end if;
    
    
end$$
delimiter ;