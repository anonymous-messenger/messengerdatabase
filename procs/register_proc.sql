

drop procedure if exists `register`;

delimiter $$
create procedure `register`(
	in `_name` varchar(128),
    in `_pass_hash` varchar(256),
    in `_salt` varchar(128),
    in `_iteration` int,
    in `_prf` int,
    in `_bytes_requested` int
)
begin
	if exists (select `username` from `user` where `user`.`username` = `_name`) then
		select "0";
    else
		insert into `pass_hash` (`hash_result`, `salt`, `iteration`, `prf`, `bytes_requested`)
        values (`_pass_hash`, `_salt`, `_iteration`, `_prf`, `_bytes_requested`);
        
        insert into `user` (`username`, `idpass_hash`) 
        values (`_name`, last_insert_id());
        
        select "1";
    end if;
end$$
delimiter ;