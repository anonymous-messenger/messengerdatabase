drop procedure if exists connectToOneOnOneChat;

delimiter $$
create procedure connectToOneOnOneChat(
	in `_name_user` varchar(256),
	in `_name_category` varchar(256)
)
begin
	declare `user_id`, `category_id`, `metrics_id` int;
    call getIdFromUserName(`_name_user`, `user_id`);
    call getIdFromCategoryName(`_name_category`, `category_id`);
    select `idvisited_categories` from `visited_categories`
    where `idcom_category` = `category_id` and `iduser` = `user_id`
    into `metrics_id`;
    
    if `metrics_id` is not null then
		update `visited_categories` set
        `last_visit` = now() 
        where `metrics_id` = `idvisited_categories`;
        
		select "1";
    else
		insert into `visited_categories`(`idcom_category`, `iduser`)
        values (`category_id`, `user_id`);
        
		select "1";
    end if;
    
    
end$$
delimiter ;