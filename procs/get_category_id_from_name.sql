drop procedure if exists getIdFromCategoryName;

delimiter $$
create procedure getIdFromCategoryName(
	in `_name_cat` varchar(256),
    out `_id_cat` int
)
begin
	select `com_category`.`idcom_category` from `com_category` 
    where `com_category`.`name` = `_name_cat` into `_id_cat`;
    
end$$
delimiter ;