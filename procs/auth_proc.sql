drop procedure if exists auth;

delimiter $$
create procedure auth(
	in `_name` varchar(128),
    in `_hash_result` varchar(256)
)
begin
	if exists (
    select `username`, `hash_result` from
    `user` as `u`  inner join `pass_hash` as `p` 
    on `u`.`idpass_hash` = `p`.`idpass_hash`
    where `p`.`hash_result` = `_hash_result` and `u`.`username` = `_name`
    ) then
		select "1";
	else
        select "0";
	end if;
end$$
delimiter ;
